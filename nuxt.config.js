export default {
  mode: 'universal',

  // Commom Head
  head: {
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Load Bar
  loading: { color: '#fff' },

  // Modules Nuxt
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    ['nuxt-i18n-module', {
      languages: ['pt', 'en', 'es']
    }],
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    '@bazzite/nuxt-optimized-images' // https://www.bazzite.com/docs/nuxt-optimized-images/
  ],

  // Global Settings

  axios: { // See https://axios.nuxtjs.org/options
    https: true,
    baseUrl: 'DOMAIN/'
  },

  // Global Import
  styleResources: { // Style
    scss: [
      'assets/sass/import.sass'
    ]
  },

  plugins: [], // Javascript

  // Build Settings
  robots: {
    UserAgent: '*',
    Allow: '/',
    Sitemap: 'https://DOMAIN/sitemap.xml'
  },
  sitemap: {
    hostname: 'https://DOMAIN',
    gzip: true,
    exclude: []
  },
  optimizedImages: {
    optimizeImages: true
  },
  build: {
    // extractCSS: true,
    extend(config, ctx) {}
  },

  // Server Settings
  server: {
    port: 8080, // default: 3000
    host: 'localhost', // default: localhost
  }

}
